var User = {};

var Form = {
    isOkay: false,
    inprogress: false,
    errorMessage: null,
    check: function() {

        var that = this;

        Form.inprogress = true;

        $form = $('#form-register');
        Form.errorMessage = $form.find('.error-message');
        Form.errorMessage.html('').hide();


        // custom fields
        var handicap = $form.find('.handicap_integer').val() + '.' + $form.find('.handicap_float').val();
        $form.find('.handicap_number').val(handicap);

        User.country = 'Belgium';

        // FORM PARAMS
        User.selected = Porscheevent.selected; // porscheevent.js
        User.contest_id = Porscheevent.selectedID;
        User.owner = $form.find('.selected_porsche_center');



        User.salutation = $form.find('.person_title');
        User.first_name = $form.find('.first_name');
        User.last_name = $form.find('.last_name');
        User.email = $form.find('.email');
        User.mobile = $form.find('.phone_number');
        User.street = $form.find('.street');
        User.no = $form.find('.address_nr');
        User.bte = $form.find('.address_bte');
        User.postcode = $form.find('.postcode');
        User.city = $form.find('.city');

        User.selected_model = $form.find('.selected_model');
        User.registration_plates = $form.find('.registration_plates');
        User.selected_porsche_center = $form.find('.selected_porsche_center').find('option:selected').html();
        User.handicap_number = $form.find('.handicap_number');
        User.federation_number = $form.find('.federation_number');
        User.club = $form.find('.club');

        User.newsletter = $form.find('.subscription_checkbox').hasClass('active') ? '1' : '0';
        // User.billing_address_checkbox = $form.find('.billing_address_checkbox');
        // billing optional form
        User.invoice = $form.find('.billing_address_checkbox').hasClass('active') ? 'company' : 'private';
        User.invoice_company = $form.find('.billing_company_name');
        User.invoice_contact = '';
        User.invoice_street = $form.find('.billing_company_address');
        User.invoice_no = $form.find('.billing_company_nr');
        User.invoice_bte = $form.find('.billing_company_bte');
        User.invoice_postcode = $form.find('.billing_company_postcode');
        User.invoice_city = $form.find('.billing_company_city');
        User.invoice_country = 'Belgium';
        User.invoice_tva = $form.find('.billing_company_vatnumber');
        User.adults = '1';

        that.setBillingForm();



        Form.isOkay = true;

        // run through all User properties
        // if field is ok then get value
        // else add error class
        var user = {};
        for (var key in User) {

            if (User.hasOwnProperty(key)) {
                if (typeof(User[key]) != 'string' && typeof(User[key]) != 'boolean') {
                    Form.checkElement(User[key], key);
                    user[key] = Form.getValue(User[key]);
                } else {
                    user[key] = User[key];
                }
            }

        }

        that.runCustomFieldsCheck();


        if (!Form.isOkay) {
            Form.error('Registration form error.');
            return;
        }

        // setTimeout(function() {
        //     console.log(user);

        //     if (Form.isOkay){
        //         console.log('FORM OK: ' + Form.isOkay);
        //     }else{
        //         console.warn('FORM OK: ' + Form.isOkay);
        //     }
        //     //Form.error();
        //     Form.finish();
        // }, 1000);
        // return;



        Service.post({
            url: $form.attr('action'),
            data: user,
            expectjson: false,
        }, function(results) {
            console.log(results);
            $('.btn_register').removeClass('inprogress').find('.loader').remove();
            that.inprogress = false;
            if (results.status == 'ok') {
                that.finish();
            } else {
                that.error(results.error);
            }
        });
    },
    setBillingForm: function() {
        if ($('.billing_address_checkbox').hasClass('active')) {

            $('.billing_company_name').removeClass('optional');
            $('.billing_company_address').removeClass('optional');
            $('.billing_company_nr').removeClass('optional');
            $('.billing_company_postcode').removeClass('optional');
            $('.billing_company_city').removeClass('optional');
            $('.billing_company_vatnumber').removeClass('optional');
        } else {
            $('.billing_company_name').addClass('optional');
            $('.billing_company_address').addClass('optional');
            $('.billing_company_nr').addClass('optional');
            $('.billing_company_postcode').addClass('optional');
            $('.billing_company_city').addClass('optional');
            $('.billing_company_vatnumber').addClass('optional');
        }

    },
    runCustomFieldsCheck: function() {

        var $handicap_int_field = $form.find('.handicap_integer');
        var $handicap_float_field = $form.find('.handicap_float');

        $handicap_float_field.removeClass('error');
        $handicap_int_field.removeClass('error');

        if ($handicap_int_field.val() == '') {
            $handicap_int_field.addClass('error');
            Form.isOkay = false;
        }

        // if ($handicap_float_field.val() == '') {
        //     Form.isOkay = false;
        //     $handicap_float_field.addClass('error');
        // }


        var handicap = parseFloat($(User.handicap_number).val());
        if (handicap < 0 || handicap > 28.4) {
            Form.isOkay = false;
            console.warn('handicap number is: ' + handicap);
            $handicap_int_field.addClass('error');
            $handicap_float_field.addClass('error');
        }

    },
    checkElement: function(element, key) {

        if ($(element).hasClass('optional') || $(element).hasClass('checkbox_wrapper')) return;

        $(element).removeClass('error');

        if ($(element).val() == false || $(element).val() == 0 || $(element).val() == '') {
            $(element).addClass('error');
            Form.isOkay = false;
        }

        if ($(element).hasClass('selectmenu')) {
            if ($('.' + key + ' :selected') == 0) {
                $(element).addClass('error');
                Form.isOkay = false;
            }
        }

        if ($(element).hasClass('email')) {
            if (!Form.validEmail($(element).val())) {
                $(element).addClass('error');
                Form.isOkay = false;
            }
        }
    },
    getValue: function(element) {
        if ($(element).hasClass('checkbox_wrapper')) {
            return $(element).hasClass('active').toString();
        } else {
            return $(element).val();
        }
    },
    validEmail: function(e) {
        var filter = /^\s*[\w\-\+_]+(\.[\w\-\+_]+)*\@[\w\-\+_]+\.[\w\-\+_]+(\.[\w\-\+_]+)*\s*$/;
        return String(e).search(filter) != -1;
    },
    finish: function() {
        Form.inprogress = false;
        $('.btn_register').removeClass('inprogress').find('.loader').remove();
        if (User.attendance == 'no') {
            $('.thank-you .attendace').addClass('hidden');
            $('.thank-you .non-attendace').removeClass('hidden');
        } else {
            $('.thank-you .attendace').removeClass('hidden');
            $('.thank-you .non-attendace').addClass('hidden');
        }
        window.location.hash = '/' + currentLANG + '/thank-you';
    },
    error: function(message) {
        Form.inprogress = false;
        $('.btn_register').removeClass('inprogress').find('.loader').remove();
        if (message == 'User is registered') {
            $(User.email).addClass('error');
        }
        //window.location.hash = '/thank-you';
    }
}
