var PopupFade = {
    $el: null,
    active: false,
    init: function() {

        var that = this;


        that.$el = $('.popup-fade');

        that.video = document.createElement('video');
        var src = 'videos/Porsche-PCBG2017.mp4';

        $(that.video).attr({ preload: 'auto', loop: false, playsinline: true, autoplay: false, src: src, controls: true, });
        that.$el.find('.video-wrapp').append(that.video);

    },
    open: function(callback) {

        PopupFade.$el.show();
        TweenMax.to(PopupFade.$el, 0.3, {
            autoAlpha: 1,
            onComplete: function() {
                PopupFade.video.play();
            }
        });


        if (callback) PopupFade.callback = callback;
        PopupFade.active = true;

    },
    close: function() {

        PopupFade.video.pause();

        TweenMax.to(PopupFade.$el, 0.3, {
            autoAlpha: 0,
            onComplete: function() {
                PopupFade.$el.hide();
            }
        });

        PopupFade.active = false;
        if (PopupFade.callback) PopupFade.callback();
    },
    reset: function() {
        PopupFade.$el.hide();
        TweenMax.set(PopupFade.$el, {
            autoAlpha: 0
        });
    }
}
