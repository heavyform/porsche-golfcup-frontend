var currentLANG = '';
var Content = {

    texts: [],
    source: '',

    supported_langs: ['nl', 'fr'],

    init: function(lang, callback) {

        if (callback) this.callback = callback;

        if (Content.texts.length > 0) {
            Content.translate(Content.texts, lang);
            return;
        }

        Service.get({
            url: 'cms/content/content.json',
            expectjson: false
        }, function(results) {
            if (results.content) {
                Content.texts = results.content.texts;
                Content.translate(results.content.texts, lang);
            }
        });

    },
    translate: function(tags, lang) {
        if (!lang) lang = 'nl';

        if (Content.supported_langs.indexOf(lang) == -1) {
            lang = 'fr';
        }

        var content = {};

        $(tags).each(function(i, tag) {
            content[tag.tag] = tag.texts[lang].replace(/\r\n|\r|\n/g, "<br />");
        });

        content['current_lang'] = lang;
        this.render(content);

    },
    render: function(content) {

        $bodyWrapp = $('.body-wrapp');
        var source = Content.source;
        if (source == '') {
            source = $bodyWrapp.html();
            Content.source = source;
        }
        var template = Handlebars.compile(source);

        var html = template(content);
        $bodyWrapp.html(html).addClass('active');

        if (this.callback) this.callback();
    }
};
