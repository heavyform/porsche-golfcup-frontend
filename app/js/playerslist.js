PlayersList = {

    loaded: false,
    rows: null,
    rows2: null,

    closed: false,

    get: function() {

        var $table = $('#playerslist-table');
        var $table2 = $('#playerslist-table2');

        if (PlayersList.closed) {
            //$table.hide();
            return;
        }


        if (PlayersList.loaded) {
            $table.append(PlayersList.rows);
            $table2.append(PlayersList.rows2);
            return;
        }



        $.get("documents/playerslist.html?ev=3", function(data) {
            PlayersList.rows = data;
            PlayersList.loaded = true;
            $table.append(PlayersList.rows);
        });

        $.get("documents/playerslist_latem.html?ev=latem", function(data) {
            PlayersList.rows2 = data;
            PlayersList.loaded = true;
            $table2.append(PlayersList.rows2);
        });
    }
}
