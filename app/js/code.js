var Code = {
    isOkey: true,
    inprogress: false,
    errorMessage: null,
    result: null,
    check: function() {

        var user = {};
        var that = this;

        User.code = $('.personal_code');

        that.inprogress = true;

        $form = $('#form-code');
        Code.errorMessage = $form.find('.error-message');
        Code.errorMessage.html('').hide();


        that.isOkay = true;
        that.checkElement(User.code, 'code');
        user.code = that.getValue(User.code);

        var distCode = user.code.substr(0, 3);
        var distTypeCode = user.code.substr(3, 1);
        // var distType = (distTypeCode == '7' || distTypeCode == '5') ? 1 : 0;

        that.checkTermOfUse();

        if (!that.isOkay) {
            Code.error();
            that.inprogress = false;
            return;
        }

        // setTimeout(function() {
        //     that.inprogress = false;
        //     Code.finish();
        // }, 1000);
        // return;

        // Service.post({
        //     url: $form.attr('action'),
        //     data: user,
        //     expectjson: true
        // }, function(results) {
        //     that.inprogress = false;
        //     if (results.status == 'ok') {
        //         that.finish();
        //     } else {
        //         that.error(results.message);
        //     }
        // });
        Service.get({
            expectjson: false,
            url: 'http://porsche-events.be/server-golfcup/public/api/json?distributor=' + distCode + '&code=' + user.code
        }, function(result) {
            console.log(result);
            that.inprogress = false;
            if (result.error) {
                that.error('Invalid code');
            } else {
                if (!result.invalid) {
                    if (!result.used) {
                        Code.result = result;
                        User.owner = result.contest[0].owner;
                        var fullCounter = 0;
                        for (var i = 0; i < 4; i++) {
                            if (result.contest[i].full) {
                                fullCounter++;
                                $('.form-event-selection.selected_event .checkbox_wrapper:nth(' + i + ')').off();
                                $('.form-event-selection.selected_event .checkbox_wrapper:nth(' + i + ')').addClass('disabled');
                            }
                        }
                        if (fullCounter == 4) {
                            $('.btn_submit_race').hide();
                        }

                        // User.contest_id = result.contest[0].id;
                        that.finish();
                    } else {
                        that.error('Used code');
                    }
                } else {
                    that.error('Invalid code');
                }
            }
        });

    },
    checkTermOfUse: function() {
        var that = this;

        $checkbox = $('.terms_of_use_checkbox');
        $checkbox.removeClass('error');

        if (!$checkbox.hasClass('active')) {
            $checkbox.addClass('error');
            that.isOkay = false;
            Code.errorMessage.html(Code.errorMessage.attr('data-rulesarenotaccepted')).show();
        }
    },
    checkElement: function(element, key) {
        var that = this;

        $(element).removeClass('error');

        if ($(element).val() === false || $(element).val() === 0 || $(element).val() === '') {
            $(element).addClass('error');
            that.isOkay = false;
        }
    },
    getValue: function(element) {
        return $(element).val();
    },
    finish: function() {
        $('.submit-code').removeClass('inprogress').find('.loader').remove();
        $(User.code).removeClass('error');
        window.location.hash = '/' + currentLANG + '/center';
    },
    error: function(message) {
        if (message === 'Invalid code') {
            Code.errorMessage.html(Code.errorMessage.attr('data-codeincorrect')).show();
        }
        if ($(User.code).val() === '') {
            Code.errorMessage.html(Code.errorMessage.attr('data-empty')).show();
        }
        if (message === 'Used code') {
            Code.errorMessage.html(Code.errorMessage.attr('data-codeused')).show();
        }
        $('.submit-code').removeClass('inprogress').find('.loader').remove();
        $(User.code).addClass('error');

    }
};
