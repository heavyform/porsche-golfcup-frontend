(function() {

    var SideMenu = function(el, settings) {

        if (!el) {
            throw new Error("Selector is missing!");
        }

        var _el = document.querySelector(el);
        if (!_el) {
            throw new Error("Selector returned empty array!");
        }

        var sidemenu = this.init(_el, settings);
        return sidemenu;

    };

    SideMenu.prototype = {
        init: function(el, settings) {
            var trackableElement;

            var wrapper = el;
            var appMenu = wrapper.querySelector(".menu-container");
            var overlay = wrapper.querySelector(".menu-background");
            var burger = document.querySelector(".menu-burger");


            var touchingElement = false;
            var startTime;
            var startX = 0,
                startY = 0;
            var currentX = 0,
                currentY = 0;

            var isOpen = false;
            var isMoving = false;

            var lastX = 0;
            var lastY = 0;
            var moveX = 0; // where in the screen is the menu currently
            var dragDirection = "";
            var maxOpacity = 0.5;

            var menuWidth = 450;
            var offset = 30;

            var init = function(element, start, move, end) {
                trackableElement = element;

                startTime = new Date().getTime(); // start time of the touch

                addEventListeners();
            };

            var addEventListeners = function() {
                trackableElement.addEventListener("touchstart", onTouchStart, false);
                trackableElement.addEventListener("touchmove", onTouchMove, false);
                trackableElement.addEventListener("touchend", onTouchEnd, false);

                overlay.addEventListener("click", closeMenu, false);
                burger.addEventListener("click", openMenu, false);
            };

            function onTouchStart(evt) {
                startTime = new Date().getTime();
                startX = evt.touches[0].pageX;
                startY = evt.touches[0].pageY;

                touchingElement = true;
                touchStart(startX, startY);

            }

            function onTouchMove(evt) {
                if (!touchingElement)
                    return;

                currentX = evt.touches[0].pageX;
                currentY = evt.touches[0].pageY;
                var translateX = currentX - startX; // distance moved in the x axis
                var translateY = currentY - startY; // distance moved in the y axis

                touchMove(evt, currentX, currentY, translateX, translateY);
            }

            function onTouchEnd(evt) {

                if (!touchingElement)
                    return;

                touchingElement = false;
                var translateX = currentX - startX; // distance moved in the x axis
                var translateY = currentY - startY; // distance moved in the y axis

                var timeTaken = (new Date().getTime() - startTime);

                touchEnd(currentX, currentY, translateX, translateY, timeTaken);
            }


            function touchStart(startX, startY) {

                isMoving = true;
                lastX = startX;
                lastY = startY;

                if (isOpen) {
                    moveX = 0;
                } else {
                    moveX = -menuWidth;
                }

                dragDirection = "";
                wrapper.classList.add("menu--visible");
            }

            function touchMove(evt, currentX, currentY, translateX, translateY) {

                if (!dragDirection) {
                    if (Math.abs(translateX) >= Math.abs(translateY)) {
                        dragDirection = "horizontal";
                    } else {
                        dragDirection = "vertical";
                    }
                }
                if (dragDirection === "vertical") {
                    lastX = currentX;
                    lastY = currentY;
                } else {
                    evt.preventDefault();

                    if (moveX + (currentX - lastX) < 0 && moveX + (currentX - lastX) > -menuWidth) {
                        moveX = moveX + (currentX - lastX);
                    }

                    lastX = currentX;
                    lastY = currentY;

                    var percentageBeforeDif = (Math.abs(moveX) * 100) / menuWidth;
                    var percentage = 100 - percentageBeforeDif;

                    var newOpacity = (((maxOpacity) * percentage) / 100);

                    if (overlay.style.opacity !== newOpacity.toFixed(2) && newOpacity.toFixed(1) % 1 !== 0) {
                        overlay.style.opacity = newOpacity.toFixed(2);
                    }

                }

                updateUi();


            }

            function touchEnd(currentX, currentY, translateX, translateY, timeTaken) {
                isMoving = false;
                var velocity = 0.2;

                if (currentX === 0 && currentY === 0) {
                    if (!isOpen) {
                        wrapper.classList.remove("menu--visible");
                    }
                } else {
                    if (isOpen) {
                        if ((translateX < (-menuWidth) / 2) || (Math.abs(translateX) / timeTaken > velocity)) {
                            if (translateX < 0) {
                                closeMenu();
                            }
                        } else {
                            openMenu();
                        }
                    } else {
                        if (translateX > menuWidth / 2 || (Math.abs(translateX) / timeTaken > velocity)) {
                            openMenu();
                        } else {
                            closeMenu();
                        }

                    }
                }
            }

            function updateUi() {
                TweenMax.to(appMenu, 0.2, {
                    x: moveX
                });
            }

            function closeMenu() {
                isOpen = false;
                overlay.style.opacity = 0;
                TweenMax.to(appMenu, 0.2, {
                    x: -(menuWidth - offset),
                    onComplete: function() {
                        wrapper.classList.remove("menu--visible");
                    }
                });
            }

            function openMenu() {
                isOpen = true;
                TweenMax.to(appMenu, 0.2, {
                    x: 0
                });
                wrapper.classList.add("menu--visible");
                overlay.style.opacity = maxOpacity;
            }


            init(appMenu);
            closeMenu();

            return {
                closeMenu: closeMenu,
                openMenu: openMenu
            }

        }
    };

    window.SideMenu = SideMenu;

})();
