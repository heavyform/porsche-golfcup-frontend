ResultsList = {

    loaded: false,
    document: '',

    closed: false,

    documents: {
        'nl': 'documents/results-nl.html?e=4',
        'fr': 'documents/results-fr.html?e=4',
    },

    get: function() {

        var that = this;
        var $table = {};

        if (ResultsList.closed) {
            return;
        }


        var $el = $('.results-append');

        // if (ResultsList.loaded) {
        //     $el.html(ResultsList.document);
        //     return;
        // }

        var path = '';
        if (currentLANG == 'nl') {
            path = that.documents.nl;
        } else {
            path = that.documents.fr;
        }

        $.get(path, function(data) {
            ResultsList.document = data;
            ResultsList.loaded = true;
            $el.html(ResultsList.document);
        });
    }
}
