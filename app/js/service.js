var Service = {};

Service.get = function(o, callback) {
    $.ajax({
        url: o.url,
        type: 'GET',
        timeout: 10000,
        success: function(data) {
            var result = {};
            if (o.expectjson) {
                try {
                    result = JSON.parse(data);
                } catch (e) {
                    result = {
                        'error': 'Could not parse data to json'
                    }
                }
                if (callback) callback(result);
            } else {
                if (callback) callback(data);
            }
        },
        error: function() {
            if (callback) callback({
                error: 'Error in connection with server'
            });
            console.error('Error in connection with server');
        }
    });
};

Service.post = function(o, callback) {
    $.ajax({
        url: o.url,
        type: 'POST',
        timeout: 10000,
        data: o.data,
        success: function(data) {
            var result = {};
            if (o.expectjson) {
                try {
                    result = JSON.parse(data);
                } catch (e) {
                    result = {
                        'error': 'Could not parse data to json'
                    }
                }
                if (callback) callback(result);
            } else {
                if (callback) callback(data);
            }
        },
        error: function() {
            if (callback) callback({
                error: 'Error in connection with server'
            });
            console.error('Error in connection with server');
        }
    });
};
