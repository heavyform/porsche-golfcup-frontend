var Porscheevent = {
    isOkey: true,
    $form: null,
    selected: '',
    selectedID: 0,
    check: function() {

        Porscheevent.$form = $('.form-event-selection');
        Porscheevent.$form.removeClass('error');

        Porscheevent.errorMessage = $('section.center').find('.error-message');
        Porscheevent.errorMessage.html('').hide();

        Porscheevent.selected = Porscheevent.getValue();
        Porscheevent.selectedID = Porscheevent.getID();

        if (Porscheevent.selected !== '') {
            Porscheevent.finish();
        } else {
            Porscheevent.error();
        }

    },
    getValue: function() {
        $selected = Porscheevent.$form.find('.checkbox_wrapper.active');
        if ($selected.length > 0) {
            return $selected.attr('data-value');
        } else {
            return '';
        }
    },
    getID: function() {
        $selected = Porscheevent.$form.find('.checkbox_wrapper.active');
        if ($selected.length > 0) {
            return $selected.attr('data-eventid');
        } else {
            return '';
        }
    },
    finish: function() {
        window.location.hash = '/' + currentLANG + '/registration';
    },
    error: function() {
        Porscheevent.errorMessage.html(Porscheevent.errorMessage.attr('data-eventnotselected')).show();
        Porscheevent.$form.addClass('error');
    }
};
