var Popup = {
    $el: null,
    active: false,
    open: function(url, callback) {
        if (callback) Popup.callback = callback;

        if (!Popup.$el) {
            Popup.$el = $('.popup .inner');
            Popup.initEvents();
        }

        $('.popup .inner').html('<div class="loader small center"></div>');
        $('body').addClass('popup-active');

        Popup.active = true;

        Service.get({
            expectjson: false,
            url: url
        }, function(result) {
            $('.popup .inner').html(result);
        });
    },
    close: function() {
        $('body').removeClass('popup-active');
        Popup.active = false;
        if (Popup.callback) Popup.callback();
    },
    initEvents: function() {
        $(document).keyup(function(e) {
            if (e.keyCode === 27) {
                Popup.close();
            }
        });
    }
}
