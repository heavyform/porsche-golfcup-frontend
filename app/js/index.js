var Site = {
    mobile: false,
    tablet: false,
    desktop: true,
    resizetimer: false,
    resizedelay: 100,
    menu: null,
    init: function() {
        if (Utility.check.phone()) {
            this.mobile = true;
            this.desktop = false;
            Site.setForMobile();
        }
        if (Utility.check.tablet()) {
            this.tablet = true;
            this.desktop = false;
            Site.setForTablet();
        }

        Site.getContent(function() {
            if (!Popup.active && currentLANG === '') {
                window.location.hash = '/languages';
            } else {
                window.location.hash = '/' + currentLANG + '/' + Site.Sections.activehref;
            }
        });

    },
    languageScreen: function() {
        window.location.hash = '/languages';
        $('.body-wrapp').addClass('active');
        Site.initEvents();
    },
    getContent: function(callback) {
        // fetch content (lang, callback)
        Content.init(currentLANG, function() {
            if (callback) {
                callback();
            } else {
                if (currentLANG !== '') {
                    window.location.hash = '/' + currentLANG + '/welcome';
                } else {
                    window.location.hash = '/' + currentLANG + '/' + Site.Sections.activehref;
                }
            }
            Site.initEvents();
            $('body').attr('data-lang', currentLANG);
            if (Site.mobile || Site.tablet) {
                Site.menu = new SideMenu('.menu-wrapper');
                $('.btn-close-menu').off().on('click', function(e) {
                    e.preventDefault();
                    Site.menu.closeMenu();
                });
            }
            // load external list
            PlayersList.get();
            ResultsList.get();

            PopupFade.init();
        });
    },
    initEvents: function() {

        $('.terms_of_use_checkbox').off().find('.checkbox').on('click', function(e) {
            $(this).parent().removeClass('error').toggleClass('active');
        });

        $('.form-event-selection .checkbox_wrapper').off().on('click', function(e) {
            $('.form-event-selection').removeClass('error');
            $('.form-event-selection .checkbox_wrapper').removeClass('active');
            $(this).addClass('active');
        });

        $('.btn_submit_race').off().on('click', function(e) {
            e.preventDefault();
            Porscheevent.check();
        });

        $('.billing_address_checkbox').off().on('click', function(e) {
            if ($(this).hasClass('active')) {
                $(this).removeClass('active');
                $('.billing-address-formpart').hide();
            } else {
                $(this).addClass('active');
                $('.billing-address-formpart').show();
            }
        });

        $('.subscription_checkbox').off().on('click', function(e) {
            $(this).removeClass('error').toggleClass('active');
        });


        $('.btn_register').off().on('click', function(e) {
            e.preventDefault();
            if (Form.inprogress) return;
            $(this).addClass('inprogress').append('<div class="loader"></div>');
            Form.check();
        });

        $('.btn_lang').off().on('click', function(e) {
            e.preventDefault();
            User.language = $(this).attr('data-lang');
            currentLANG = User.language;
            Site.getContent();
        });

        $('.btn_lang_change').off().on('click', function(e) {
            e.preventDefault();

            User.language = $(this).attr('data-lang');
            currentLANG = User.language;

            Site.getContent(function() {
                Site.Sections.reset();
                $('.body-wrapp').addClass('active');
                Site.initEvents();
                $(window).scrollTop(0);
            });
        });

        $('.submit-code').off().on('click', function(e) {
            e.preventDefault();
            if (Code.inprogress) return;
            $(this).addClass('inprogress').append('<div class="loader"></div>');
            Code.check();
        });

    },
    setForMobile: function() {
        $('body').removeClass('desktop').addClass('sidemenu-init mobile');
        $('meta[name=viewport]').attr('content', 'width=640, user-scalable=no');
    },
    setForTablet: function() {
        $('body').removeClass('desktop').addClass('sidemenu-init tablet');
        $('meta[name=viewport]').attr('content', 'width=768, user-scalable=no');
    }
};

Site.Sections = {
    activehref: '',
    change: function(section) {
        $('section').removeClass('active');
        if (section == 'registration' || section == 'thank-you') {
            if (!User.code || User.code === '') {
                window.location.hash = '';
                Site.Sections.change('welcome');
                return;
            }
            $('section.' + section).addClass('active');
            Site.Sections.activehref = section;
            Site.Sections.updateMenu();
        } else {
            $('section.' + section).addClass('active');
            Site.Sections.activehref = section;
            Site.Sections.updateMenu();
        }
        $('html, body').scrollTop(0);
        if (Popup.active) {
            Popup.close();
        }

        if (Site.menu) {
            Site.menu.closeMenu();
        }
    },
    reset: function() {
        $('section').removeClass('active');
        $('section.' + Site.Sections.activehref).addClass('active');
    },
    updateMenu: function(section) {
        if (Site.Sections.activehref == 'languages') {
            $('nav.translated').hide();
            $('.menu-burger').hide();
        } else {
            $('nav.translated').show();
            if (!Site.desktop) $('.menu-burger').show();
        }
    }
};

// RULES DEEPLINK
// Hashbang.map('/rules/nl', function() {
//     Popup.open('documents/info_and_rules_nl.html', function() {
//         window.location.hash = '/' + Site.Sections.activehref;
//     });
// });
// Hashbang.map('/rules/fr', function() {
//     Popup.open('documents/info_and_rules_fr.html', function() {
//         window.location.hash = '/' + Site.Sections.activehref;
//     });
// });

Hashbang.map('<lang>/<section>', function() {
    if (currentLANG === '') {
        currentLANG = this.lang;
    } else {
        if (currentLANG != this.lang) {
            currentLANG = this.lang;
            Site.getContent(function() {});
        }
    }
    if (this.section === '') this.section = 'welcome';
    Site.Sections.change(this.section);
});


Hashbang.map('/<section>', function() {
    Site.Sections.change(this.section);
});

Hashbang.map('/', function() {
    Site.Sections.change('languages');
});



$(document).ready(function() {
    Site.init();
});
