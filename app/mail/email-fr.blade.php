<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>{{$subject}}</title>
    <!--[if mso]>
    <style type="text/css">
    body, table, td {font-family: Arial, Helvetica, sans-serif !important;}
    table tr {padding: 15px 0px;}
    </style>
    <![endif]-->
</head>

<body style="width: 600px;margin:0px auto;padding:0px;position: relative;font-family: Arial, Helvetica, sans-serif;">
    <table border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td style="display: block;height: 40px;width: 600px;margin-top:0px;line-height: 40px;background-color: #fff;color: #9d9d9d;font-size: 13px; " align="center">
                <a href="http://porsche-events.be/server-golfcup/public/viewemail?owner={{$owner}}&contest_id={{$contest_id}}&last_name={{$name}}&salutation={{$salutation}}&language={{$language}}" target="_blank" style="text-decoration: underline;color:#9d9d9d;display:block;text-align:center; font-size: 11px "> View mail in browser</a>
            </td>
        </tr>
        <tr>
            <td style="position: relative;display: block;height: auto;padding:0px;margin:0px;width: 600px;">
                <img src="http://porsche-events.be/golfcup/mail/img/logo.jpg" style="width: 600px;" alt="porsche events be">
            </td>
        </tr>
        <tr>
            <td style="position: relative;display: block;height:auto;padding:0px;margin:0px;width: 600px;">
                <img src="http://porsche-events.be/golfcup/mail/img/header.jpg" style="width: 600px;" align="Porsche Family Reunion">
            </td>
        </tr>
        <tr>
            <td style="width: 600px;position: relative;display: block;padding:0px;line-height: 16px;border-bottom: 1px solid #ccc;">
                <table border="0" cellspacing="0" cellpadding="0" width="100%">
                    <td style="width: 380px;position: relative;;padding:0px;line-height: 16px;">
                        <p style="font-size: 22px;font-weight: bold;letter-spacing: -1px;text-align:left;margin-left:20px;">
                            <br/>Porsche Golf Cup 2017
                        </p>
                        <br/>
                    </td>
                    <td style="width: 220px;position: relative;padding:0px;line-height: 16px;" align="right">
                        <img src="http://porsche-events.be/golfcup/mail/img/cup_logo.jpg " style="width: 160px;margin:5px auto; display: block " alt="Porsche Golf Cup " align="right">
                        <br/>
                    </td>
                </table>
            </td>
        </tr>
        <tr>
            <td style="width: 600px;position: relative;display: block;margin:0px auto;padding:0px;line-height: 16px; ">
                <p style="font-size: 20px;font-weight: bold;letter-spacing: -1px; margin-left:20px;margin-right:20px;">
                    <br/>Confirmation
                    <br/>
                    <br/>
                </p>
            </td>
        </tr>
        <tr>
            <td style="width: 600px;position: relative;display: block;margin:0px auto;padding:0px;line-height: 16px; ">
                <p style="font-size: 14px;font-weight: bold;margin:0px; margin-left:20px;margin-right:20px;">
                    @if ($salutation == 'Dhr' || $salutation == 'Mr')
                    Cher Monsieur {{$name}},
                    @else
                    Ch&#232;re Madame {{$name}},
                    @endif
                    <br/>
                    <br/>
                </p>
            </td>
        </tr>
        <tr>
            <td style="width: 600px;position: relative;display: block;margin:0px auto;padding:0px;line-height: 16px; ">
                <p style="font-size: 14px;margin:0px;margin-left:20px;margin-right:20px;">
                    Nous avons le plaisir de vous confirmer votre inscription pour l’épreuve de qualification à la Porsche Golf Cup 2017. <br><br>
                    Date: {{$date_of_the_event}} <br>
                    Club {{$name_of_the_event}} <br>
                    {{$address_event}}
                    <br/>
                    <br/> 
                    Toutes les informations pratiques sont disponibles sur notre site <a href="http://www.porschegolfcup.be">www.porschegolfcup.be</a> <br>
                    <br>
                    Nous vous invitons à consulter notre site une semaine avant la date d’épreuve, pour consulter le programme de la journée et votre heure de départ. <br>
                    <br>
                    Nous restons à votre disposition pour toutes questions complémentaires. <br>
                    <br>
                    Au plaisir de partager cette journée de qualification entre Porschistes.
                    <br/>
                    <br/>
                    <br/>L'Equipe Porsche,
                    <br/>
                    <br/>
                    {{$distributor}}<br>
                    {{$distributor_address}}

                </p>
            </td>
        </tr>
        <!-- <tr>
            <td style="width: 600px;position: relative;display: block;margin:0px auto;padding:0px;line-height: 16px; " align="center">
                <br/>
                &nbsp;
                <br/>
                <img src="http://porsche-events.be/golfcup/mail/img/sponsors.jpg" style="width: 540px;" alt="Porsche Golf Cup Sponsors">
                <br/>
                &nbsp;
                <br/>
            </td>
        </tr> -->
        <tr>
            <td style="display: block;height: 40px;width: 600px;margin-top:50px;line-height: 40px;background-color: #ffffff;color: #9d9d9d;font-size: 13px; " align="center">
                <a style="text-decoration: underline;color:#9d9d9d;display:block;text-align:center; font-size: 11px " href="http://www.porschegolfcup.be" target="_blank ">http://www.porschegolfcup.be</a>
            </td>
        </tr>
    </table>
</body>

</html>
