var app = app || angular.module('contentApp', []);

// content
app.controller('contentCtrl', function($scope, $http) {

    var defaultLanguages = [{
        'lang': 'nl'
    }];

    $scope.content = {};
    $scope.edited = 'false';
    $scope.loading = 'false';

    var jsonloaded = false;

    function init(content) {
        $scope.content = {
            languages: content.languages,
            texts: content.texts.reverse()
        };
        resetNew();
    }


    function resetNew() {
        $scope.newlang = '';
        $scope.newlangmessage = '';
        $scope.newText = {
            tag: '',
            texts: {}
        };
        angular.forEach($scope.content.languages, function(lang, key) {
            $scope.newText.texts[lang.lang] = '';
        });
    }


    function load() {
        if (jsonloaded) return;
        $scope.loading = 'true';
        $http({
            method: 'GET',
            url: 'content/content.json'+'?hash_id=' + Math.random(),
            cache: false,
        }).then(function successCallback(response) {
            if (response.data.content) {
                init(response.data.content);
            } else {
                init({
                    languages: defaultLanguages,
                    texts: []
                });
            }
            $scope.loading = 'false';
            jsonloaded = true;

        }, function errorCallback(response) {
            init({
                languages: defaultLanguages,
                texts: []
            });
            $scope.loading = 'false';
            jsonloaded = true;
        });
    }
    load();


    function save(data) {
        $scope.loading = 'true';
        $http({
            method: 'POST',
            url: 'content/savecontent.php',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
            },
            data: {
                content: data
            }
        }).then(function successCallback(response) {
            //console.log(response);
            resetNew();
            $scope.loading = 'false';

        }, function errorCallback(response) {
            //console.log(response);
            $scope.loading = 'false';
        });
    }


    // add new lang
    $scope.addLang = function() {

        if ($scope.newlang == '') {
            $scope.newlangmessage = 'Please add language id tag.';
            return;
        }

        $scope.content.languages.push({
            'lang': $scope.newlang
        });

        // add new lang in new text form
        $scope.newText.texts[$scope.newlang] = '';

        // add new lang in existing texts
        angular.forEach($scope.content.texts, function(text, key) {
            text.texts[$scope.newlang] = '';
        });

        // save
        save($scope.content);
    };


    // add text
    $scope.addText = function() {
        if ($scope.newText.tag == '') {
            $scope.newtextgmessage = 'Please add text id tag.';
            return;
        }

        $scope.content.texts.unshift({
            tag: $scope.newText.tag,
            texts: $scope.newText.texts
        });

        // save
        save($scope.content);
    };

    // update text
    $scope.updateText = function() {
        $scope.edited = 'false';

        // save
        save($scope.content);
    };

    $scope.removeText = function() {

    };


    $scope.showSaveBtn = function() {
        $scope.edited = 'true';
    }

});
