<?php

function printResult($output){
	echo json_encode($output);
	exit;
}

$json = file_get_contents('php://input');

if(!isset($json) || $json == ''){
	printResult(array('error','No json data'));
};

$fp = fopen('content.json', 'w');
fwrite($fp, $json);
fclose($fp);

printResult(array('status', 'ok'));


?>