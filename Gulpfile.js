var gulp = require('gulp');
var concatCss = require('gulp-concat-css');
var cleanCSS = require('gulp-clean-css');
//var sass = require('gulp-sass');
var concat = require('gulp-concat');
var browserSync = require('browser-sync').create();
var usemin = require('gulp-usemin');
var rev = require('gulp-rev');
var filter = require('gulp-filter');
var gutil = require('gulp-util');
var path = require('path');
var newer = require('gulp-newer');


gulp.task('copy', function() {
    var fileFilter = filter(function(file) {
        return file.stat.isFile();
    });
    return gulp.src([
            path.join('./app/', '/**/*'),
            path.join('!' + './app/', '/**/*.{html,js,scss,css}'),
            path.join('!./app/cms/', '/**/*')
        ])
        .pipe(fileFilter)
        .pipe(newer('./dist/'))
        .pipe(gulp.dest(path.join('./dist/', '/')));
});


gulp.task('copydocuments', function() {
    return gulp.src([
            path.join('./app/documents/playerslist.html'),
            path.join('./app/documents/results.html')
        ])
        .pipe(newer('./dist/documents'))
        .pipe(gulp.dest(path.join('./dist/documents', '/')));
});


gulp.task('build', ['copy', 'copydocuments'], function() {
    return gulp.src('app/index.html')
        .pipe(usemin({
            js: [],
            js1: []
        }))
        .pipe(gulp.dest('./dist/'));
});

gulp.task('default', ['build']);
